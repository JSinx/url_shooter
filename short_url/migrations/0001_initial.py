# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2017-11-26 08:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ShortUrl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(db_index=True, unique=True, verbose_name='site url')),
                ('visits', models.IntegerField(verbose_name='visit counter')),
                ('created_at', models.DateTimeField(editable=False, verbose_name='created datetime')),
            ],
        ),
    ]
