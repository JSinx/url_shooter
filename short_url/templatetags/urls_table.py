from django import template

register = template.Library()


@register.inclusion_tag('urls_table.html', takes_context=True)
def urls_table_show(context, urls_list):
    return {'context': context, 'urls_list': urls_list}
