from django.conf.urls import url

from . import views

app_name = 'short_url'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^list$', views.list, name='list'),
    url(r'^(?P<short_url_id>[0-9]+)/$', views.open_url, name='open_url'),
    url(r'^detail/(?P<short_url_id>[0-9]+)/$', views.detail, name='url_detail'),
    url(r'^add_url$', views.add_url, name='add_url'),
]
