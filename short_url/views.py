from django.shortcuts import render, redirect,  get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import ShortUrl
from .forms import CreateShortUrlForm


def index(request):
    popular_urls = ShortUrl.ordered_urls()[:20]
    return render(request, 'short_url/index.html', {'popular_urls': popular_urls})


def open_url(request, short_url_id):
    short_url = get_object_or_404(ShortUrl, pk=short_url_id)
    short_url.visits += 1
    short_url.save()
    return redirect(short_url.url)


def detail(request, short_url_id):
    short_url = get_object_or_404(ShortUrl, pk=short_url_id)
    full_url = request.build_absolute_uri(short_url.get_absolute_url())
    return render(request, 'short_url/detail.html', {'short_url': short_url, 'url': full_url})


def add_url(request):
    form = CreateShortUrlForm(request.POST)
    if not (request.method == 'POST' and form.is_valid()):
        return redirect(request.path_info)

    short_url, created = ShortUrl.objects.get_or_create(url=request.POST['url'])
    return render(request, 'short_url/detail.html', {'short_url': short_url})


def list(request):
    urls_list = ShortUrl.ordered_urls()
    page = request.GET.get('page', 1)

    paginator = Paginator(urls_list, 10)

    try:
        urls_list = paginator.page(page)
    except PageNotAnInteger:
        urls_list = paginator.page(1)
    except EmptyPage:
        urls_list = paginator.page(paginator.num_pages)

    return render(request, 'short_url/list.html', {'urls_list': urls_list})
