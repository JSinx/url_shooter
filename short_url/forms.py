from django import forms

from .models import ShortUrl


class CreateShortUrlForm(forms.Form):
    url = forms.CharField(max_length=255, required=True)
