from django.db import models
from django.utils import timezone


class ShortUrl(models.Model):
    url = models.URLField('site url', db_index=True, unique=True)
    visits = models.IntegerField('visit counter', default=0)
    created_at = models.DateTimeField('created datetime', editable=False)

    def save(self, *args, **kwargs):
        if self.created_at is None:
            self.created_at = timezone.now()

        return super(ShortUrl, self).save(*args, **kwargs)

    def __str__(self):
        return self.url

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('short_url:url_detail', args=[self.id])

    @classmethod
    def ordered_urls(cls):
        return cls.objects.order_by('-visits', '-created_at')