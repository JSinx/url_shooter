from django.test import TestCase, Client
from .models import ShortUrl
from .forms import CreateShortUrlForm


class ShortFormTestCase(TestCase):
    def test_fill_url(self):
        form = CreateShortUrlForm(data={'url': 'ya.ru'})
        self.assertTrue(form.is_valid())

    def test_empty_url(self):
        form = CreateShortUrlForm(data={'url': ''})
        self.assertFalse(form.is_valid())

    def test_long_url(self):
        form = CreateShortUrlForm(data={'url': 'c'*256})
        self.assertFalse(form.is_valid())


class ShortUrlTestCase(TestCase):
    def test_visits_is_zero(self):
        short_url = ShortUrl.objects.create(url="http://ya.ru")
        self.assertEqual(short_url.visits, 0)



class ShortUrlViewTestCase(TestCase):
    def setUp(self):
        # Every test needs a client.
        self.short_url = ShortUrl.objects.create(url="http://ya.ru", visits=10)
        self.client = Client()

    def test_details(self):
        response = self.client.get('/detail/{}'.format(self.short_url.id), follow=True)
        self.assertEqual(response.status_code, 200)


    def test_redirect(self):
        response = self.client.get('/{}'.format(self.short_url.id), follow=True)
        self.assertEqual(response.status_code, 200)
        short_url = ShortUrl.objects.get(pk=self.short_url.id)
        self.assertEqual(short_url.visits, 11)

    def test_create_new_url(self):
        url = 'http://google.ru'
        self.client.post('/add_url', data={'url': url})
        short_urls = ShortUrl.objects.filter(url=url)
        self.assertEqual(len(short_urls), 1)
